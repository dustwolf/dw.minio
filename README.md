# dw.minio

Ansible role for installing Minio server.

Currently made to work with Ubuntu Linux.

## Project status
This is a work in progress. I am using this to manage my own private servers and I will contribute as time allows.

You may use this code if you find it useful.
